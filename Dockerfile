FROM openjdk:11-alpine
ENTRYPOINT ["/usr/bin/testolo.sh"]

COPY testolo.sh /usr/bin/testolo.sh
COPY target/testolo.jar /usr/share/testolo/testolo.jar
